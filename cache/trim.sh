#!/bin/bash

: ${RUSTUP_HOME:=${HOME}/.rustup} ${CARGO_HOME:=${HOME}/.cargo}
RUSTUP_OLD="$(dirname "$RUSTUP_HOME")/old_$(basename "$RUSTUP_HOME")"
CARGO_OLD="$(dirname "$CARGO_HOME")/old_$(basename "$CARGO_HOME")"
find "$1"/{.rustup,.cargo} -mount -print0 | r="$1" \
    perl -0 -ne 'BEGIN { %ids; }; chomp; my $id = (lstat($_))[1];'\
' if (exists $ids{$id}) { my $s = $ids{$id}; print STDOUT "$s\0$_\0";'\
' $s =~ s%^\Q$ENV{r}\E/.rustup(.*)$%\Q$ENV{RUSTUP_OLD}\E$1%;'\
' $s =~ s%^\Q$ENV{r}\E/.cargo(.*)$%\Q$ENV{CARGO_OLD}\E$1%;'\
' unlink $_; -e $s && unlink $s; } else { $ids{$id} = $_; };' \
        >"$1"/hardlinks.dat
g=$(pwd)
(
    cd "$1"/.rustup
    diff -qsra "$RUSTUP_OLD" . | grep -ve'^Only' -e' differ$' |  cut -d\  -f4 |  xargs rm -f
) &
(
    cd "$1"/.cargo
    diff -qsra "$CARGO_OLD" . | grep -ve'^Only' -e' differ$' | cut -d\  -f4 |  xargs rm -f
)
wait
find "$1"/{.rustup,.cargo} -mount -print0 |
    perl -0 -ne 'chomp; my @s = lstat($_); print "$s[8]/$s[9]/$_\0";' \
        >"$1"/dates.dat
exit 0
