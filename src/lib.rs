//    Marika On-Demand Channel.
//    Copyright 2023 Michael Mestnik

//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Library General Public
//    License as published by the Free Software Foundation; either
//    version 2 of the License.

//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//    Library General Public License for more details.

//    You should have received a copy of the GNU Library General Public
//    License along with this library; if not, write to the
//    Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
//    Boston, MA  02110-1301, USA.

//    Copyright 2023 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#![feature(duration_constants)]

use std::collections::VecDeque;
use std::sync::{mpsc, Arc};

pub struct Channel<T, F> {
    d: VecDeque<mpsc::Receiver<T>>,
    f: Arc<F>,
}

impl<T, F> Channel<T, F>
where
    T: 'static + Send,
    F: 'static + Sync + Send + Fn() -> T,
{
    pub fn new(f: F) -> Self {
        let mut this = Self {
            d: VecDeque::new(),
            f: f.into(),
        };
        this.fill();
        this
    }

    pub fn with_capacity(f: F, capacity: usize) -> Self {
        let mut this = Self {
            d: VecDeque::with_capacity(capacity),
            f: f.into(),
        };
        this.fill();
        this
    }

    fn free(&self) -> usize {
        self.d.capacity() - self.d.len()
    }

    fn fill(&mut self) {
        let count = self.free();
        for _ in 0..count {
            let (s, r) = mpsc::sync_channel(self.d.capacity());
            let f = self.f.clone();
            std::thread::spawn(move || s.send((f)()).unwrap());
            self.d.push_back(r);
        }
    }

    pub fn pop<const N: u32, const D: u32>(&mut self) -> Result<T, mpsc::RecvTimeoutError> {
        let t = match self.d.pop_front() {
            Some(x) => x
                .recv_timeout(std::time::Duration::MILLISECOND)
                .or_else(|x| {
                    if matches!(x, mpsc::RecvTimeoutError::Timeout) {
                        Ok((self.f)())
                    } else {
                        Err(x)
                    }
                }),
            None => Ok((self.f)()),
        };
        if self.d.capacity() as f64 / self.free() as f64 <= N as f64 / (D as f64 + 0.1) {
            self.fill();
        }
        t
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {}
}
